#!/usr/bin/env python3
from PyPgf import pgf

axisflags = ["cycle list name=color list",
             "domain=-6:6",
             "legend pos=outer north east",
             "title=trigonometric functions",
             "xlabel=$x$","ylabel=$f(x)$",
             ]

plt = pgf(axisflags) # Initialize the plot with the above flags.
# You should also be able to pass a single string as a flag if all you want is
# a single flag.

plotflags = ["no marks","samples=100"] # These flags are passed to the \addplot
plt.plot_function("10*sin(deg(x))",'sin(x)',plotflags+["blue"])
plt.plot_function("10*cos(deg(x))",'cos(x)',plotflags+["red"]) 
plt.plot_function("tan(deg(x))",'tan(x)',plotflags+["green"])
# The first option is the function plotted, the second one is the label for the
# legend, and the last is the flags for the \addplot
# The syntax for plot_point2 plot_point3 plot_curve2 plot_curve3 is similar,
# only the type of the first option changes.

plt.end() # Tells the object that we won't add any more things. This ends the
          # axis and tikzpicture environment.
          # If all you want is the tikzpicture environment to maybe \input into
          # your document, save after this.
plt.wrap() # This wraps a standalone type document around the plot.
plt.save('plot.tex') # This saves the document to plot.tex
plt.comp() # This compiles the document. By default it uses pdflatex.
