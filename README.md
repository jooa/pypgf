# README #

PyPgf is a python interface that uses pgfplots to create a LaTeX plot.

### ToDo ###

* Create a better documentation
* Add a thing for stuff
* The other thing
* Make a better ToDo list

### Usage ###

For an example see example.py

### Licensing ###

PyPgf is licensed under the MIT license.
See LICENSE for details.

![Example plot](https://bytebucket.org/jooa/pypgf/raw/75af4eb8928a0480e87c05df1c4fef1de8b57a44/plot.svg)
