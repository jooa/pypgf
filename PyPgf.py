#!/usr/bin/env python3
import os

class pgf:
    def __init__(self,flags=[]):
        """Create a plots list and append some beginning things to it"""
        if type(flags) == str():
            flags = [flags]
        self.plots = []
        self.plots.append("\\begin{tikzpicture}")
        self.plots.append("\\begin{axis}[")
        for flag in flags:
            self.plots.append('\t%s,' % flag)
        self.plots.append("]")
        self.legend = []


    def plot_function(self,function,label="",flags=[]):
        """Takes a mathematical function in pgfplots syntax and plots it on the
        axis. Also optionally uses flags, and accepts a label for the
        legend."""
        if type(flags) == str:
            flags = [flags]
        self.legend += [label]
        self.plots.append("\\addplot[")
        for flag in flags:
                self.plots.append("\t%s," % flag)
        self.plots.append("]{%s};" % function)

    def plot_point2(self,point,label="",flags=[]):
        """Takes a 2-3 point and label for the legend and plots it on the thingy.
        Also accepts optional flags"""
        if type(flags) == str():
            flags = [flags]
        self.legend += [label]
        self.plots.append('\\addplot+[')
        for flag in flags:
            self.plots.append('\t%s,' % flag)
        self.plots.append('] coordinates {')
        if len(point) == 3: self.plots.append("(%f,%f)[%f]" % point)
        elif len(point) == 2: self.plots.append("(%f,%f)" % point)
        else: raise(ValueError,"Must have at two or three numbers in point")
        self.plots.append('};')

    def plot_point3(self,point,label="",flags=[]):
        """Takes a 3-4 point and label for the legend and plots it on the thingy.
        Also accepts optional flags"""
        if type(flags) == str():
            flags = [flags]
        self.legend += [label]
        self.plots.append('\\addplot+[')
        for flag in flags:
            self.plots.append('\t%s,' % flag)
        self.plots.append('] coordinates {')
        if len(point) == 4: self.plots.append("(%f,%f,%f)[%f]" % point)
        elif len(point) == 3: self.plots.append("(%f,%f,%f)" % point)
        else: raise(ValueError,"Must have three or four numbers in point")
        self.plots.append('};')

    def plot_curve2(self,points,label="",flags=[]):
        """Takes a series of points and plots them as a line on the thingy.
        Also takes a label for the legend and optional flags."""
        if type(flags) == str():
            flags = [flags]
        self.legend += [label]
        flags += []
        self.plots.append('\\addplot+[')
        for flag in flags:
            self.plots.append('\t%s,' % flag)
        self.plots.append('] coordinates {')
        for point in points:
            if len(point) == 2: self.plots.append("(%f,%f)" % point)
            elif len(point) == 3: self.plots.append("(%f,%f) [%f]" % point)
            else: raise(ValueError,"Must have 2 or 3 numbers in point")
        self.plots.append('};')

    def plot_curve3(self,points,label="",flags=[]):
        """Takes a series of points and plots them as a line on the thingy.
        Also takes a label for the legend and optional flags."""
        if type(flags) == str():
            flags = [flags]
        self.legend += [label]
        flags += []
        self.plots.append('\\addplot+[')
        for flag in flags:
            self.plots.append('\t%s,' % flag)
        self.plots.append('] coordinates {')
        for point in points:
            if len(point) == 3: self.plots.append("(%f,%f,%f)" % point)
            elif len(point) == 4: self.plots.append("(%f,%f,%f) [%f]" % point)
            else: raise(ValueError,"Must have 3 or 4 numbers in point")
        self.plots.append('};')

    def end(self):
        """Writes the legend and ends the axis and tikzpicture environments."""
        self.plots.append('\\legend{')
        for l in self.legend:
            self.plots.append('%s,' % l)
        self.plots.append('}')
        self.plots.append("\\end{axis}")
        self.plots.append("\\end{tikzpicture}")

    def wrap(self):
        """Wraps the thing into a document"""
        wrap_begin = ["\\documentclass{standalone}","\\usepackage{pgfplots}","\\usepackage{mathtools}","\\pgfplotsset{compat=1.12}","\\begin{document}"]
        wrap_end = ["\\end{document}"]
        self.plots = wrap_begin + self.plots + wrap_end

    def save(self,file_l):
        """Saves the plot to a file"""
        self.file_l = file_l
        self.result = os.linesep.join(self.plots)
        with open(file_l,'w') as f: f.write(self.result)

    def comp(self,compiler="pdflatex"):
        """Compiles the document using compiler and things"""
        self.file_r = '.'.join(self.file_l.split('.')[:-1]).split('/')[-1]
        os.system('pdflatex %s' % self.file_l)
        os.remove(self.file_r+'.aux')
        os.remove(self.file_r+'.log')
        os.rename(self.file_r+'.pdf','.'.join(self.file_l.split('.')[:-1])+'.pdf')

